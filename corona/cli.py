"""Console script for corona."""
import argparse
from datetime import datetime, timedelta

from corona import get_stats


def argument_parser():
    parser = argparse.ArgumentParser(description="Corona Virus (COVID-19) stats")
    parser.add_argument(
        "-c",
        "--country",
        help="The country to retrieve the stats from (e.g. Finland)",
        required=True,
    )
    yesterday = (datetime.now() - timedelta(days=1)).date()
    yesterday_iso = yesterday.isoformat()
    parser.add_argument(
        "-d",
        "--date",
        help=f"The country to retrieve the stats from (e.g. {yesterday_iso})",
        default=yesterday_iso,
    )
    args = parser.parse_args()
    return args


def print_stats(stats, date):
    """Prints stats dictionary to the console."""
    print(f"Date: {date}")
    for category, value in stats.items():
        print(f"{category.capitalize()}: {value}")


def main():
    """Console script for corona."""
    args = argument_parser()
    country = args.country
    date = datetime.fromisoformat(args.date).date()
    stats = get_stats(country, date)
    print_stats(stats, date)


if __name__ == "__main__":
    main()  # pragma: no cover
