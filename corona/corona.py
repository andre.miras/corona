"""Main module."""
import csv

import requests

BASE_URL = (
    "https://raw.githubusercontent.com"
    "/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/"
)
LINKS = {
    "confirmed": "time_series_covid19_confirmed_global.csv",
    "death": "time_series_covid19_deaths_global.csv",
    "recovered": "time_series_covid19_recovered_global.csv",
}
CSV_DATE_FORMAT = "%-m/%-d/%y"


def country_to_indexes(dict_list, country):
    """
    From a given list of dictionary and country, returns the indexes of the country.
    """
    return [
        i
        for i, d in enumerate(dict_list)
        if d["Country/Region"].lower() == country.lower()
    ]


def download_content(csv_file):
    """Downloads and returns the content of the remote CSV file."""
    url = f"{BASE_URL}{csv_file}"
    response = requests.get(url)
    return response.content.decode()


def get_active(confirmed, recovered, death):
    """Computes the active cases given the stats."""
    return confirmed - recovered - death


def get_stats(country, date):
    """Returns the confirmed, death and recovered stats for a given country and date."""
    stats = {}
    for category, csv_file in LINKS.items():
        csv_content = download_content(csv_file)
        reader = csv.DictReader(csv_content.splitlines())
        dict_list = list(reader)
        country_indexes = country_to_indexes(dict_list, country)
        values = [
            int(dict_list[i][date.strftime(CSV_DATE_FORMAT)]) for i in country_indexes
        ]
        value = sum(values)
        stats = dict(stats, **{category: value})
    active = get_active(**stats)
    stats = dict(stats, **{"active": active})
    return stats
