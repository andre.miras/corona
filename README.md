# Corona Stats CLI

Command Line Interface to the Corona virus stats (COVID-19)


## Usage

```text
python corona/cli.py --help                                        
usage: corona/cli.py [-h] -c COUNTRY [-d DATE]

Corona Virus (COVID-19) stats

optional arguments:
  -h, --help            show this help message and exit
  -c COUNTRY, --country COUNTRY
                        The country to retrieve the stats from (e.g. Finland)
  -d DATE, --date DATE  The country to retrieve the stats from (e.g. 2020-03-27)
```
